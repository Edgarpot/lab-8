#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "constants.h"

struct date
{
    int day;
    int month;
    int year;
};

struct oper
{
    char chet[MAX_STRING_SIZE];
    char summa[MAX_STRING_SIZE];
    char naz[MAX_STRING_SIZE];
};

struct book_subscription
{
    oper reader;
    date start;
    char vid[MAX_STRING_SIZE];
};

#endif
